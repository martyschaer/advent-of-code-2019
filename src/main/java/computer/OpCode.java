package computer;

import lombok.Builder;
import lombok.Value;

import java.util.HashMap;
import java.util.Map;

@Value
@Builder
public class OpCode {
    private final int code;
    private final int argc;
    private final Mode[] mode;

    public static OpCode parse(int input) {
        OpCodeBuilder builder = OpCode.builder();
        int code = input % 100; // the two least significant digits are the opcode
        builder.code(code);

        switch (code) {
            case 1:
            case 2:
            case 7:
            case 8:
                builder.argc(3);
                break;
            case 5:
            case 6:
                builder.argc(2);
                break;
            case 3:
            case 4:
            case 9:
                builder.argc(1);
                break;
            case 99:
                builder.argc(0);
                break;
            default:
                throw new IllegalArgumentException(input + " is an unknown OpCode!");
        }

        int strlen = builder.argc + 2;
        String str = Integer.toString(input);

        switch (builder.argc) {
            case 0:
                builder.mode(new Mode[0]);
            case 1:
                builder.mode(new Mode[]{detectMode(str, strlen, 0)}); //
                break;
            case 2:
                builder.mode(new Mode[]{ //
                        detectMode(str, strlen, 1), //
                        detectMode(str, strlen, 0) //
                });
                break;
            case 3:
                builder.mode(new Mode[]{ //
                        detectMode(str, strlen, 2), //
                        detectMode(str, strlen, 1), //
                        detectMode(str, strlen, 0) //
                });
                break;
            default:
                throw new IllegalArgumentException("No known commands with " + builder.argc + " arguments");
        }

        return builder.build();
    }

    private static Mode detectMode(String str, int strlen, int pos) {
        switch (charAt(str, strlen, pos)) {
            case '0':
                return Mode.POSITION;
            case '1':
                return Mode.IMMEDIATE;
            case '2':
                return Mode.RELATIVE;
        }

        return Mode.POSITION;
    }

    private static char charAt(String string, int strlen, int pos) {
        pos += (string.length() - strlen);
        if(pos < 0) {
            return '0';
        }
        return string.charAt(pos);
    }

    public Mode getMode(int argp) {
        return this.mode[argp];
    }
}
