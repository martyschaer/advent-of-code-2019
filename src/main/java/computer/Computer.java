package computer;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

public class Computer {
    private Map<Long, Long> memory = new ConcurrentHashMap<>();
    private final BlockingQueue<Long> input;
    private final BlockingQueue<Long> output;
    private int relativeBase = 0;
    private boolean debug;
    private long ip = 0;
    private final boolean returningMode;

    private static final int ADD = 1;
    private static final int MUL = 2;
    private static final int INP = 3;
    private static final int OUT = 4;
    private static final int JIT = 5;
    private static final int JIF = 6;
    private static final int LTH = 7;
    private static final int EQU = 8;
    private static final int REL = 9;
    private static final int HLT = 99;

    public Computer(final long[] memory, BlockingQueue<Long> input, BlockingQueue<Long> output) {
        this(memory, input, output, false);
    }

    public Computer(final long[] memory, BlockingQueue<Long> input, BlockingQueue<Long> output, boolean returningMode) {
        for(int i = 0; i < memory.length; i++) {
            this.memory.put((long)i, memory[i]);
        }
        this.input = input;
        this.output = output;
        this.debug = false;
        this.returningMode = returningMode;
    }

    public Computer(final Map<Long, Long> memory, BlockingQueue<Long> input, BlockingQueue<Long> output, boolean returningMode) {
        this.memory = memory;
        this.input = input;
        this.output = output;
        this.debug = false;
        this.returningMode = returningMode;
    }

    public Computer clone() {
        Computer copy = new Computer(new HashMap<>(memory), new LinkedBlockingQueue<>(), new LinkedBlockingQueue<>(), this.returningMode);
        copy.ip = this.ip;
        copy.relativeBase = this.relativeBase;
        return copy;
    }

    public BlockingQueue<Long> getInput() {
        return input;
    }

    public BlockingQueue<Long> getOutput() {
        return output;
    }

    public int execute() {
        try {
            while(true) {
                OpCode op = OpCode.parse((int)(long) memory.get(ip));
                debug(ip, op.getArgc());
                ++ip;
                switch (op.getCode()) {
                    case HLT:
                        return ExitCode.HALT;
                    case ADD:
                        runThreeParamInstruction(op.getMode(), ip, (a, b) -> a + b);
                        break;
                    case MUL:
                        runThreeParamInstruction(op.getMode(), ip, (a, b) -> a * b);
                        break;
                    case INP:
                        long addr = fetchAddr(op.getMode(0), memory.get(ip));
                        if(returningMode && input.isEmpty()) {
                            --ip;
                            return ExitCode.INPUT_INTERRUPT;
                        }
                        memory.put(addr, input.take());
                        break;
                    case OUT:
                        long value = fetch(op.getMode(0), memory.get(ip));
                        output.offer(value);
                        break;
                    case JIT:
                        long testJit = fetch(op.getMode(0), memory.get(ip));
                        if (testJit != 0) {
                            ip = fetch(op.getMode(1), memory.get(ip + 1));
                            continue;
                        }
                        break;
                    case JIF:
                        long testJif = fetch(op.getMode(0), memory.get(ip));
                        if (testJif == 0) {
                            ip = fetch(op.getMode(1), memory.get(ip + 1));
                            continue;
                        }
                        break;
                    case LTH:
                        runThreeParamInstruction(op.getMode(), ip, (a, b) -> a < b ? 1L : 0L);
                        break;
                    case EQU:
                        runThreeParamInstruction(op.getMode(), ip, (a, b) -> a.equals(b) ? 1L : 0L);
                        break;
                    case REL:
                        long offset = fetch(op.getMode(0), memory.get(ip));
                        relativeBase += offset;
                        break;
                }

                ip += op.getArgc();
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private void debug(long ip, int argc) {
        if(this.debug) {
            System.out.printf("%04d@[%s]%n", ip, LongStream.range(ip, ip + argc + 1).map(x -> memory.computeIfAbsent(x, k -> 0L)).mapToObj(Long::toString).collect(Collectors.joining("|")));
            if(memory.computeIfAbsent(ip, k -> 0L) % 10 == 4) {
                System.out.println("output");
            }
        }
    }

    private void runThreeParamInstruction(Mode[] modes, long ip, BiFunction<Long, Long, Long> operation) {
        long a = fetch(modes[0], memory.get(ip));
        long b = fetch(modes[1], memory.get(ip + 1));
        long addr = fetchAddr(modes[2], memory.get(ip + 2));
        memory.put(addr, operation.apply(a, b));
    }

    private long fetchAddr(Mode mode, long arg) {
        if(mode == Mode.POSITION) {
            return arg;
        } else if(mode == Mode.RELATIVE) {
            return relativeBase + arg;
        } else {
            throw new IllegalStateException();
        }
    }

    private long fetch(Mode mode, long arg) {
        if (mode == Mode.IMMEDIATE) {
            return arg;
        } else if(mode == Mode.POSITION) {
            return this.memory.computeIfAbsent(arg, k -> 0L);
        } else if(mode == Mode.RELATIVE) {
            return this.memory.computeIfAbsent(relativeBase + arg, k -> 0L);
        }
        throw new IllegalStateException();
    }
}
