package util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileUtil {
    public static String loadFileFromPath(Path path) {
        try {
            return new String(Files.readAllBytes(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String loadResource(String name) {
        return loadFileFromPath(getResourcePath(name));
    }

    public static Path getResourcePath(String name) {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        return Paths.get(new File(classLoader.getResource(name).getFile()).getAbsolutePath());
    }

}
