package day3;

import java.awt.*;
import java.util.*;

public class Move {
    private char direction;
    private int distance;

    private static final Map<Character, Point> points = new HashMap<>();

    static {
        points.put('U', new Point(0, -1));
        points.put('D', new Point(0, 1));
        points.put('L', new Point(-1, 0));
        points.put('R', new Point(1, 0));
    }

    public Move(String move) {
        this.direction = move.charAt(0);
        this.distance = Integer.parseInt(move.substring(1));
    }

    public Stack<Point> applyTo(Stack<Point> path) {
        Point dir = points.get(direction);
        for (int i = 0; i < distance; i++) {
            Point head = path.peek();
            path.push(new Point(dir.x + head.x, dir.y + head.y));
        }
        return path;
    }
}
