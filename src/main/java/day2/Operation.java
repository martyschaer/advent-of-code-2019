package day2;

import lombok.Data;
import lombok.Value;

@Value
public class Operation {
    private OpCode opCode;
    private int addrA;
    private int addrB;
    private int addrDst;

    public void execute(int[] memory) {
        if(OpCode.ADD.equals(opCode)) {
            memory[addrDst] = memory[addrA] + memory[addrB];
        } else if(OpCode.MUL.equals(opCode)) {
            memory[addrDst] = memory[addrA] * memory[addrB];
        }
    }
}
