package day2;

public enum OpCode {
    ADD(1),
    MUL(2),
    HLT(99);

    int val;

    OpCode(int val) {
        this.val = val;
    }

    public static OpCode ofVal(int val) {
        switch (val) {
            case 1:
                return ADD;
            case 2:
                return MUL;
            case 99:
                return HLT;
            default:
                throw new IllegalArgumentException("Unknown OpCode " + val);
        }
    }
}
