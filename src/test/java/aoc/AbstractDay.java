package aoc;

import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.time.Duration;
import java.time.Instant;

public abstract class AbstractDay {
    public abstract void part1(String input);
    public abstract void part2(String input);
    public void tearDown() throws Exception {
        // NOP
    }

    public void cleanupBetweenParts() {
        // NOP
    }

    private OutputStream out = new ByteOutputStream();


    public void test(int day, String input) {
        test(day, input, false);
    }

    public void test(int day, String input, boolean debug) {
        if(!debug) {
            //warmup
            System.setOut(new PrintStream(out));

            for(int i = 0; i < 100; i++) {
                part1(input);
                cleanupBetweenParts();
                part2(input);
            }

            int benchMarkIterations = 100;

            Duration part1SumTime = Duration.ZERO;
            Duration part2SumTime = Duration.ZERO;

            for(int i = 0; i < benchMarkIterations; i++) {
                part1SumTime = part1SumTime.plus(time(() -> part1(input)));
                cleanupBetweenParts();
                part2SumTime = part2SumTime.plus(time(() -> part2(input)));
            }

            System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

            part1(input);
            System.out.printf("---%nDay %d, Part 1: %s%n---%n", day, Util.toHumanReadable(part1SumTime.dividedBy(benchMarkIterations)));

            part2(input);
            System.out.printf("---%nDay %d, Part 2: %s%n---%n", day, Util.toHumanReadable(part2SumTime.dividedBy(benchMarkIterations)));
        } else {
            part1(input);
            System.out.printf("---%nDay %d, Part 1: %s%n---%n", day, "no measurement");

            part2(input);
            System.out.printf("---%nDay %d, Part 2: %s%n---%n", day, "no measurement");
        }

        try {
            System.out.println("Cleaning up");
            tearDown();
            System.out.println("Cleanup done");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private Duration time(Runnable runnable) {
        Instant start = Instant.now();
        runnable.run();
        Instant end = Instant.now();

        return Duration.between(start, end);
    }
}
