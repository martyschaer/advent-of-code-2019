package aoc.day03;

import aoc.AbstractDay;
import day3.Move;
import util.FileUtil;

import java.awt.*;
import java.util.List;
import java.util.*;

public class Day_03 extends AbstractDay {
    public static void main(String[] args) throws Exception{
        String input = FileUtil.loadResource("day03.txt");
        Day_03 day = new Day_03();
        day.test(3, input);
    }

    @Override
    public void part1(String input) {
        String[] wireInputs = input.split("\n");
        HashSet<Point> wire0 = new HashSet<>(trace(wireInputs[0]));
        HashSet<Point> wire1 = new HashSet<>(trace(wireInputs[1]));

        Point centralPort = new Point(0, 0);

        wire0.retainAll(wire1);
        wire0.remove(centralPort);

        Optional<Integer> closest = wire0.stream() //
                .map(this::manhattanDistance) //
                .sorted() //
                .findFirst();

        if(closest.isPresent()) {
            System.out.printf("The closest intersection is md(%d) away%n", closest.get());
        } else {
            System.out.println("No solution found.");
        }

    }

    @Override
    public void part2(String input) {
        String[] wireInputs = input.split("\n");
        List<Point> wire0 = trace(wireInputs[0]);
        List<Point> wire1 = trace(wireInputs[1]);

        Point centralPort = new Point(0, 0);
        Set<Point> intersections = new HashSet<>(wire0);
        intersections.retainAll(new HashSet<>(wire1));
        intersections.remove(centralPort);

        Map<Point, Integer> wire0Steps = calculateDistances(wire0, intersections);
        Map<Point, Integer> wire1Steps = calculateDistances(wire1, intersections);

        OptionalInt shortestSteps = intersections.stream() //
                .mapToInt(intersection -> wire0Steps.get(intersection) + wire1Steps.get(intersection)) //
                .min();

        System.out.printf("The fewest combined steps are %d%n", shortestSteps.getAsInt());
    }

    private Map<Point, Integer> calculateDistances(List<Point> path, Set<Point> intersections) {
        Map<Point, Integer> distances = new HashMap<>();
        for(int steps = 0; steps < path.size(); steps++) {
            if(intersections.contains(path.get(steps))) {
                distances.put(path.get(steps), steps);
            }
        }
        return distances;
    }


    private int manhattanDistance(Point a) {
        return Math.abs(a.x) + Math.abs(a.y);
    }

    private List<Point> trace(String wire) {
        Stack<Point> path = new Stack<>();
        path.push(new Point(0, 0));

        for(String moveDescription : wire.split(",")) {
            Move move = new Move(moveDescription);
            path = move.applyTo(path);
        }

        return new ArrayList<>(path);
    }
}
