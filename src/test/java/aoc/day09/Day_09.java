package aoc.day09;

import aoc.AbstractDay;
import computer.Computer;
import util.FileUtil;

import java.util.concurrent.*;

public class Day_09 extends AbstractDay {
    public static void main(String[] args) throws Exception{
        String input = FileUtil.loadResource("day09.txt");
        Day_09 day = new Day_09();
        day.test(9, input);
    }

    @Override
    public void part1(String input) {
        long[] memory = parseInput(input);
        BlockingQueue<Long> inputQueue = new LinkedBlockingQueue<>();
        BlockingQueue<Long> outputQueue = new LinkedBlockingQueue<>();

        inputQueue.add(1L);

        System.out.printf("Running with input %s. ", inputQueue);

        Computer computer = new Computer(memory, inputQueue, outputQueue);

        computer.execute();

        System.out.printf("Output is %s.%n", outputQueue);
    }

    @Override
    public void part2(String input) {
        long[] memory = parseInput(input);
        BlockingQueue<Long> inputQueue = new LinkedBlockingQueue<>();
        BlockingQueue<Long> outputQueue = new LinkedBlockingQueue<>();

        inputQueue.add(2L);

        System.out.printf("Running with input %s. ", inputQueue);

        Computer computer = new Computer(memory, inputQueue, outputQueue);

        computer.execute();

        System.out.printf("Output is %s.%n", outputQueue);
    }

    private long[] parseInput(String input) {
        String[] tokens = input.trim().split(",");
        long[] memory = new long[tokens.length];
        for(int i = 0; i < tokens.length; i++) {
            memory[i] = Long.parseLong(tokens[i]);
        }

        return memory;
    }
}
