package aoc.day11;

import aoc.AbstractDay;
import aoc.Util;
import computer.Computer;
import util.FileUtil;

import java.awt.*;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * Panels painted at least once: 2629
 * ---
 * Day 11, Part 1: 71.500 millis
 * ---
 * .#..#.####..##..####.#..#.###..#....###....
 * .#..#....#.#..#.#....#.#..#..#.#....#..#...
 * .#..#...#..#..#.###..##...###..#....#..#...
 * .#..#..#...####.#....#.#..#..#.#....###....
 * .#..#.#....#..#.#....#.#..#..#.#....#......
 * ..##..####.#..#.####.#..#.###..####.#......
 *
 * ---
 * Day 11, Part 2: 6.600 millis
 * ---
 */
public class Day_11 extends AbstractDay {
    public static void main(String[] args) throws Exception{
        String input = FileUtil.loadResource("day11.txt");
        Day_11 day = new Day_11();
        day.test(11, input);
    }

    private final Map<Long, Map<Long, Integer>> panels = new HashMap<>();
    private final Set<Point> panelsPainted = new HashSet<>();

    @Override
    public void part1(String input) {
        runRobot(input, 0);

        System.out.printf("Panels painted at least once: %d%n", panelsPainted.size());
    }

    @Override
    public void tearDown() {
        this.panelsPainted.clear();
        this.panels.clear();
    }

    @Override
    public void cleanupBetweenParts() {
        tearDown();
    }

    @Override
    public void part2(String input) {
        this.panelsPainted.clear();
        this.panels.clear();

        runRobot(input, 1);
        renderOutput();
    }

    private void runRobot(String input, long initialColor) {
        long[] program = parseInput(input);
        BlockingQueue<Long> inputs = new LinkedBlockingQueue<>();
        BlockingQueue<Long> output = new LinkedBlockingQueue<>();
        Computer robot = new Computer(program, inputs, output);

        Thread runner = new Thread(robot::execute);
        runner.start();

        Direction facing = Direction.UP;

        long xPos = 0;
        long yPos = 0;
        boolean firstIteration = true;
        try{
            while (runner.isAlive()) {
                inputs.put(firstIteration ? initialColor : readPanel(xPos, yPos));
                Long color = output.poll(10, TimeUnit.MILLISECONDS);
                if(color == null && !runner.isAlive()) {
                    break;
                }
                paintPanel(xPos, yPos, color);
                long steering = output.take();
                facing = facing.steer(steering);
                xPos += facing.x;
                yPos += facing.y;
                firstIteration = false;
            }
            runner.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void renderOutput() {
        long maxX = this.panels.keySet().stream().mapToLong(Long::longValue).max().getAsLong();
        long minX = this.panels.keySet().stream().mapToLong(Long::longValue).min().getAsLong();

        Set<Long> yKeys = this.panels.values().stream().flatMap(e -> e.keySet().stream()).collect(Collectors.toSet());
        long maxY = yKeys.stream().mapToLong(Long::longValue).max().getAsLong();
        long minY = yKeys.stream().mapToLong(Long::longValue).min().getAsLong();

        StringBuilder sb = new StringBuilder();
        for(long y = minY; y <= maxY; y++) {
            for(long x = minX; x <= maxX; x++) {
                sb.append(readPanel(x, y) == 0 ? "  " : "##");
            }
            sb.append("\n");
        }
        System.out.println(sb.toString());
    }

    private long readPanel(long x, long y) {
        return panels.computeIfAbsent(x, k -> new HashMap<>()).computeIfAbsent(y, k -> 0);
    }

    private void paintPanel(long x, long y, long color) {
        panelsPainted.add(new Point((int)x, (int)y));
        panels.computeIfAbsent(x, k -> new HashMap<>()).put(y, (int)color);
    }

    private long[] parseInput(String input) {
        String[] tokens = input.trim().split(",");
        long[] memory = new long[tokens.length];
        for(int i = 0; i < tokens.length; i++) {
            memory[i] = Long.parseLong(tokens[i]);
        }

        return memory;
    }

    enum Direction {
        UP(0, -1),
        DOWN(0, 1),
        LEFT(-1, 0),
        RIGHT(1, 0);

        public int x;
        public int y;

        Direction(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public Direction steer(long in) {
            switch (this) {
                case UP:
                    return in == 0 ? LEFT : RIGHT;
                case LEFT:
                    return in == 0 ? DOWN : UP;
                case DOWN:
                    return in == 0 ? RIGHT : LEFT;
                case RIGHT:
                    return in == 0 ? UP : DOWN;
            }
            throw new IllegalStateException("Couldn't figure out next move.");
        }
    }
}
