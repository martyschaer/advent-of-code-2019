package aoc.day12;

import aoc.AbstractDay;
import util.FileUtil;

import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Day_12 extends AbstractDay {
    public static void main(String[] args) throws Exception{
        String input = FileUtil.loadResource("day12.txt");
        Day_12 day = new Day_12();
        day.test(12, input);
    }

    private Pattern inputRegex = Pattern.compile("^<x=(?<x>-?\\d+), y=(?<y>-?\\d+), z=(?<z>-?\\d+)>$");

    public List<Moon> parseInput(String input) {
        return Arrays.stream(input.split("\n")) //
            .map(line -> {
                Matcher matcher = inputRegex.matcher(line);
                matcher.matches();
                int x = Integer.parseInt(matcher.group("x"));
                int y = Integer.parseInt(matcher.group("y"));
                int z = Integer.parseInt(matcher.group("z"));
                return new ThreeTuple<>(x, y, z);
            }).map(Moon::new).collect(Collectors.toList());
    }

    @Override
    public void part1(String input) {
        List<Moon> moons = parseInput(input);
        int steps = 1000;
        for(int i = 0; i < steps; i++) {
            step(moons);
        }
        double totalEnergy = moons.stream().mapToDouble(Moon::getEnergy).sum();
        System.out.printf("Total energy in the system after %d steps is %.03f%n", steps, totalEnergy);
    }

    @Override
    public void part2(String input) {
        List<Moon> moons = parseInput(input);
        long steps = 0;
        long xPeriodicity = -1;
        long yPeriodicity = -1;
        long zPeriodicity = -1;
        while (xPeriodicity < 0 || yPeriodicity < 0 || zPeriodicity < 0) {
            steps++;
            step(moons);
            int xVel = 0, yVel = 0, zVel = 0;
            for(Moon moon : moons) {
                xVel += Math.abs(moon.velocity.x);
                yVel += Math.abs(moon.velocity.y);
                zVel += Math.abs(moon.velocity.z);
            }
            if(xPeriodicity < 0 && xVel == 0) xPeriodicity = steps;
            if(yPeriodicity < 0 && yVel == 0) yPeriodicity = steps;
            if(zPeriodicity < 0 && zVel == 0) zPeriodicity = steps;
        }

        System.out.printf("lcm(%d, %d, %d) * 2 = %d%n", xPeriodicity, yPeriodicity, zPeriodicity, lcm(xPeriodicity, yPeriodicity, zPeriodicity) * 2);
    }

    private static long lcm(long... values) {
        long lcm = values[0];
        for(int i = 1; i < values.length; i++) {
            lcm = lcm(lcm, values[i]);
        }
        return lcm;
    }

    private static long lcm(long a, long b) {
        return Math.abs(a * b) / gcd(a, b);
    }

    private static long gcd(long a, long b) {
        if(b == 0) return a;
        return gcd(b, a % b);
    }

    private void step(List<Moon> moons) {
        for(Moon moon : moons) {
            for(Moon attractor : moons) {
                if(attractor == moon) {
                    continue;
                }
                moon.velocity.x += attractor.position.x.compareTo(moon.position.x);
                moon.velocity.y += attractor.position.y.compareTo(moon.position.y);
                moon.velocity.z += attractor.position.z.compareTo(moon.position.z);
            }
        }

        for(Moon moon : moons) {
            moon.position.x += moon.velocity.x;
            moon.position.y += moon.velocity.y;
            moon.position.z += moon.velocity.z;
        }
    }

    private static class Moon{
        public ThreeTuple<Integer> position;
        public ThreeTuple<Integer> velocity;

        public Moon(ThreeTuple<Integer> initialPosition) {
            this.position = initialPosition;
            this.velocity = new ThreeTuple<>(0, 0, 0);
        }

        public double getEnergy() {
            return position.magnitude() * velocity.magnitude();
        }

        @Override
        public String toString() {
            return "Moon{" +
                    "position=" + position +
                    ", velocity=" + velocity +
                    '}';
        }
    }

    private static class ThreeTuple<T extends Number> {
        public T x;
        public T y;
        public T z;

        public ThreeTuple(T x, T y, T z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ThreeTuple<?> that = (ThreeTuple<?>) o;

            if (!Objects.equals(x, that.x)) return false;
            if (!Objects.equals(y, that.y)) return false;
            return Objects.equals(z, that.z);
        }

        @Override
        public int hashCode() {
            int result = x != null ? x.hashCode() : 0;
            result = 31 * result + (y != null ? y.hashCode() : 0);
            result = 31 * result + (z != null ? z.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return String.format("(%s, %s, %s)", x, y, z);
        }

        public double magnitude() {
            return Math.abs(x.doubleValue()) + Math.abs(y.doubleValue()) + Math.abs(z.doubleValue());
        }
    }
}
