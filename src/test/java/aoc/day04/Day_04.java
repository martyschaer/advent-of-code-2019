package aoc.day04;

import aoc.AbstractDay;
import util.FileUtil;

import java.util.stream.IntStream;

public class Day_04 extends AbstractDay {
    public static void main(String[] args) throws Exception {
        String input = FileUtil.loadResource("day04.txt");
        Day_04 day = new Day_04();
        day.test(4, input);
    }

    @Override
    public void part1(String input) {
        String[] in = input.split("-");
        int start = Integer.parseInt(in[0]);
        int end = Integer.parseInt(in[1]);

        long candidates = IntStream.rangeClosed(start, end) //
                .parallel() //
                .mapToObj(Integer::toString) //
                .map(String::toCharArray) //
                .filter(this::isCandidatePassword) //
                .count();

        System.out.printf("There are %d candidates.%n", candidates);
    }

    private boolean isCandidatePassword(char[] candidate) {
        boolean sawPair = false;
        for (int i = 1; i < candidate.length; ++i) {
            if (candidate[i - 1] == candidate[i]) {
                sawPair = true;
            } else if(candidate[i - 1] > candidate[i]) {
                return false;
            }
        }
        return sawPair;
    }

    @Override
    public void part2(String input) {
        String[] in = input.split("-");
        int start = Integer.parseInt(in[0]);
        int end = Integer.parseInt(in[1]);

        long candidates = IntStream.rangeClosed(start, end) //
                .parallel() //
                .mapToObj(Integer::toString) //
                .map(String::toCharArray) //
                .filter(this::increasing) //
                .filter(this::isCandidatePasswordWithMoreInformation) //
                .count();

        System.out.printf("There are %d candidates.%n", candidates);
    }

    private boolean increasing(char[] candidate) {
        for (int i = 1; i < candidate.length; ++i) {
            if(candidate[i - 1] > candidate[i]) {
                return false;
            }
        }
        return true;
    }

    private boolean isCandidatePasswordWithMoreInformation(char[] candidate) {
        int[] pairOccurrences = new int[10];
        for (int i = 1; i < candidate.length; ++i) {
            if (candidate[i - 1] == candidate[i]) {
                ++pairOccurrences[candidate[i] - 0x30];
            }
        }

        for(int occurrences : pairOccurrences) {
            if(occurrences == 1) {
                return true;
            }
        }

        return false;
    }
}
