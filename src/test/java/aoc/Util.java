package aoc;

import java.time.Duration;

public class Util {
    public static String toHumanReadable(Duration duration) {
        try{
            long nanos = duration.toNanos();
            if(nanos > 1_500_000_000) {
                throw new ArithmeticException();
            }
            return String.format("%.03f %s", nanos / 1_000_000.0, "millis");
        } catch (ArithmeticException e) {
            long millis = duration.toMillis();
            return String.format("%.03f %s", millis / 1000.0, "seconds");
        }
    }
}
