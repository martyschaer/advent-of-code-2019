package aoc;

import util.FileUtil;

import java.util.Arrays;

public class Day_xx extends AbstractDay {
    public static void main(String[] args) throws Exception{
        String input = FileUtil.loadResource("dayxx.txt");
        Day_xx day = new Day_xx();
        day.test(0, input);
    }

    @Override
    public void part1(String input) {

    }

    @Override
    public void part2(String input) {

    }
}
