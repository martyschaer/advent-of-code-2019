package aoc.day14;

import aoc.AbstractDay;
import ds.Tuple;
import util.FileUtil;

import java.util.*;
import java.util.function.BiFunction;

public class Day_14 extends AbstractDay {
    public static void main(String[] args) throws Exception {
        String input = FileUtil.loadResource("debug.txt");
        Day_14 day = new Day_14();
        day.test(14, input, true);
    }

    private Map<String, Tuple<Integer, Set<Tuple<String, Integer>>>> parseInput(String input) {
        Map<String, Tuple<Integer, Set<Tuple<String, Integer>>>> result = new HashMap<>();
        for (String line : input.split("\n")) {
            String[] tokens = line.split("=>");
            Tuple<String, Integer> resultingChemical = parseSingle(tokens[1]);
            result.put(resultingChemical.getLeft(), new Tuple<Integer, Set<Tuple<String, Integer>>>(resultingChemical.getRight(), new HashSet<>()));
            for (String parentChemical : tokens[0].split(",")) {
                result.get(resultingChemical.getLeft()).getRight().add(parseSingle(parentChemical));
            }
        }
        return result;
    }

    private Tuple<String, Integer> parseSingle(String token) {
        String[] tokens = token.trim().split(" ");
        return new Tuple<>(tokens[1], Integer.parseInt(tokens[0]));
    }

    private Map<String, Integer> chemicalPool = new HashMap<>();
    private Map<String, Tuple<Integer, Set<Tuple<String, Integer>>>> reactions;

    @Override
    public void part1(String input) {
        chemicalPool.clear();
        reactions = parseInput(input);
        Map<String, Integer> cost = getCost("FUEL", 1);
        System.out.println(cost);
        System.out.println(chemicalPool);

        // "return" unused chemicals from the pool
        int oreToReturn = chemicalPool.entrySet().stream() //
                .filter(e -> e.getValue() > 0) //
                .filter(e -> getMOQ(e.getKey()) <= e.getValue()) // can we even return anything?
                .filter(e -> 0 < (e.getValue() - (e.getValue() % getMOQ(e.getKey()))))
                .map(e -> getCost(e.getKey(), e.getValue() - e.getValue() % getMOQ(e.getKey()))) //
                .map(m -> m.get("ORE")) //
                .filter(Objects::nonNull) //
                .mapToInt(Integer::intValue) //
                .sum();

        System.out.println(oreToReturn);
        System.out.printf("Required ORE for 1 FUEL is %d.%n", cost.get("ORE") - oreToReturn);
    }

    public Map<String, Integer> getCost(String resultingChemical, int requiredAmount) {
        Map<String, Integer> result = new HashMap<>();
        if (resultingChemical.equals("ORE")) {
            //result.put("ORE", requiredAmount);
            return result;
        }

        Tuple<Integer, Set<Tuple<String, Integer>>> integerSetTuple = reactions.get(resultingChemical);
        int recipeAmount = integerSetTuple.getLeft();
        Set<Tuple<String, Integer>> requirements = integerSetTuple.getRight();

        int numberOfRecipesRequired = 1;
        int a = recipeAmount;
        while(a < requiredAmount) { a+=recipeAmount;numberOfRecipesRequired++; }

        for (Tuple<String, Integer> requirement : requirements) {
            int amountOfIngredientRequired = roundUpBy(requirement.getRight() * numberOfRecipesRequired, getMOQ(requirement.getLeft()));
            Map<String, Integer> costs = getCost(requirement.getLeft(), amountOfIngredientRequired);
            costs.put(requirement.getLeft(), requirement.getRight() * numberOfRecipesRequired);
            sumToMap(chemicalPool, requirement.getLeft(), amountOfIngredientRequired - requirement.getRight() * numberOfRecipesRequired);
            costs.put(requirement.getLeft(), amountOfIngredientRequired);
            costs.forEach((key, value) -> sumToMap(result, key, value));
        }
        return result;
    }

    private void sumToMap(Map<String, Integer> target, String key, int value) {
        if(target.containsKey(key)) {
            target.computeIfPresent(key, (k, v) -> target.get(key) + value);
        } else {
            target.put(key, value);
        }
    }

    private int roundUpBy(int toRound, double increment) {
        return (int) (increment * Math.ceil(toRound / increment));
    }

    private int getMOQ(String chemical) {
        if(reactions.containsKey(chemical)) {
            return reactions.get(chemical).getLeft();
        }
        return 1;
    }


    @Override
    public void part2(String input) {

    }

    private class Chemical {
        private final String name;
        private final int amount;
        private final Set<Chemical> parents = new HashSet();

        public Chemical(String name, int amount) {
            this.name = name;
            this.amount = amount;
        }

        public void addParent(Chemical parent) {
            this.parents.add(parent);
        }

        public Set<Chemical> getParents() {
            return this.parents;
        }

        public String getName() {
            return name;
        }

        public int getAmount() {
            return amount;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Chemical chemical = (Chemical) o;

            if (amount != chemical.amount) return false;
            if (name != null ? !name.equals(chemical.name) : chemical.name != null) return false;
            return parents != null ? parents.equals(chemical.parents) : chemical.parents == null;
        }

        @Override
        public int hashCode() {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + amount;
            result = 31 * result + (parents != null ? parents.hashCode() : 0);
            return result;
        }
    }
}
