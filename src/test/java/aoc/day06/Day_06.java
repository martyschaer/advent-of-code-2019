package aoc.day06;

import aoc.AbstractDay;
import util.FileUtil;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Day_06 extends AbstractDay {
    public static void main(String[] args) throws Exception {
        String input = FileUtil.loadResource("day06.txt");
        Day_06 day = new Day_06();
        day.test(6, input);
    }

    private static final String CENTER_OF_MASS = "COM";
    private static final String YOU = "YOU";
    private static final String SANTA = "SAN";
    private final Map<String, String> orbits = new HashMap<>();
    private final Map<String, Integer> memoization = new HashMap<>();

    private void parseMap(String input) {
        // keeping the memoization over multiple benchmark runs is cheating
        orbits.clear();
        memoization.clear();

        Arrays.stream(input.split("\n")) //
                .filter(s -> !s.isEmpty())
                .forEach(data -> {
                    orbits.put(data.substring(4), data.substring(0, 3));
                });
    }

    @Override
    public void part1(String input) {
        parseMap(input);
        int totalOrbits = orbits.keySet().stream() //
                .mapToInt(this::countOrbits) //
                .sum();

        System.out.printf("The total number of orbits is %d%n", totalOrbits);
    }

    private int countOrbits(String body) {
        if (body.equals(CENTER_OF_MASS)) {
            return 0;
        }
        return 1 + memoization.computeIfAbsent(body, b -> countOrbits(orbits.get(b)));
    }

    @Override
    public void part2(String input) {
        parseMap(input);
        Stack<String> sanPath = new Stack<>();
        Stack<String> youPath = new Stack<>();

        traceRouteToCOM(sanPath, orbits.get(SANTA));
        traceRouteToCOM(youPath, orbits.get(YOU));

        while (sanPath.peek().equals(youPath.peek())) {
            sanPath.pop();
            youPath.pop();
        }

        System.out.printf("Shortest path between )YOU and )SAN is %d hops%n", sanPath.size() + youPath.size());
    }

    private void traceRouteToCOM(Stack<String> path, String body) {
        path.push(body);
        if(!body.equals(CENTER_OF_MASS)) {
            traceRouteToCOM(path, orbits.get(body));
        }
    }
}
