package aoc.day05;

import aoc.AbstractDay;
import computer.Computer;
import util.FileUtil;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Day_05 extends AbstractDay {
    public static void main(String[] args) throws Exception {
        String input = FileUtil.loadResource("day05.txt");
        Day_05 day = new Day_05();
        day.test(5, input);
    }

    @Override
    public void part1(String input) {
        String[] tokens = input.trim().split(",");
        long[] memory = new long[tokens.length];
        for (int i = 0; i < tokens.length; i++) {
            memory[i] = Long.parseLong(tokens[i]);
        }

        BlockingQueue<Long> inputQueue = new LinkedBlockingQueue<>();
        inputQueue.add(1L);
        BlockingQueue<Long> outputQueue = new LinkedBlockingQueue<>();

        System.out.printf("Running with input %s. ", inputQueue);

        Computer computer = new Computer(memory, inputQueue, outputQueue);

        computer.execute();

        System.out.printf("Output is %s.%n", outputQueue);
    }

    @Override
    public void part2(String input) {
        String[] tokens = input.trim().split(",");
        long[] memory = new long[tokens.length];
        for (int i = 0; i < tokens.length; i++) {
            memory[i] = Long.parseLong(tokens[i]);
        }

        BlockingQueue<Long> inputQueue = new LinkedBlockingQueue<>();
        inputQueue.add(5L);
        BlockingQueue<Long> outputQueue = new LinkedBlockingQueue<>();

        System.out.printf("Running with input %s. ", inputQueue);

        Computer computer = new Computer(memory, inputQueue, outputQueue);

        computer.execute();

        System.out.printf("Output is %s.%n", outputQueue);
    }
}
