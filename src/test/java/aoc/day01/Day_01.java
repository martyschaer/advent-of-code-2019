package aoc.day01;

import aoc.AbstractDay;
import util.FileUtil;

import java.util.Arrays;

public class Day_01 extends AbstractDay {
    public static void main(String[] args) throws Exception{
        String input = FileUtil.loadResource("day01.txt");
        Day_01 day = new Day_01();
        day.test(1, input);
    }

    @Override
    public void part1(String input) {
        int fuelRequirements = Arrays.stream(input.split("\\v")) //
                .mapToInt(Integer::parseInt) //
                .map(i -> i / 3) //
                .map(i -> i - 2) //
                .sum();
        System.out.println(fuelRequirements);
    }

    @Override
    public void part2(String input) {
        int fuelRequirements = Arrays.stream(input.split("\\v")) //
                .mapToInt(Integer::parseInt) //
                .map(i -> i / 3) //
                .map(i -> i - 2) //
                .map(Day_01::calcFuelRequirements) //
                .sum();
        System.out.println(fuelRequirements);
    }

    public static int calcFuelRequirements(int mass) {
        int fuelMass = (mass / 3) - 2;
        if(fuelMass <= 0) {
            return mass;
        }
        return mass + calcFuelRequirements(fuelMass);
    }
}
