package aoc.day07;

import aoc.AbstractDay;
import computer.Computer;
import util.FileUtil;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;

public class Day_07 extends AbstractDay {
    public static void main(String[] args) throws Exception{
        String input = FileUtil.loadResource("day07.txt");
        Day_07 day = new Day_07();
        day.test(7, input);
    }

    private ExecutorService executor = Executors.newFixedThreadPool(20);

    @Override
    public void tearDown() throws Exception{
        executor.shutdown();
        executor.awaitTermination(5, TimeUnit.SECONDS);
    }

    @Override
    public void part1(String input) {
        long[] memory = parseInput(input);

        long maxThrust = createPermutations(new int[]{0, 1, 2, 3, 4}).stream() //
                .parallel() //
                .mapToLong(phaseSettings -> runPhaseSettings(memory, phaseSettings)) //
                .max().getAsLong();

        System.out.printf("Maximum thrust is %d%n", maxThrust);
    }

    @Override
    public void part2(String input) {
        long[] memory = parseInput(input);

        long maxThrust = createPermutations(new int[]{5, 6, 7, 8, 9}).stream() //
                .parallel() //
                .mapToLong(phaseSettings -> runPhaseSettings(memory, phaseSettings)) //
                .max().getAsLong();

        System.out.printf("Maximum thrust using feedback amplification is %d%n", maxThrust);
    }

    private long[] parseInput(String input) {
        String[] tokens = input.trim().split(",");
        long[] memory = new long[tokens.length];
        for(int i = 0; i < tokens.length; i++) {
            memory[i] = Long.parseLong(tokens[i]);
        }

        return memory;
    }

    private List<int[]> createPermutations(int[] values) {
        List<int[]> result = new LinkedList<>();
        permutations(values.length, values, result);
        return result;
    }

    private void permutations(int k, int[] arr, List<int[]> result) {
        if(k == 1) {
            result.add(Arrays.copyOf(arr, arr.length));
        } else {
            permutations(k - 1, arr, result);

            for(int i = 0; i < k - 1; i++) {
                if(k % 2 == 0) {
                    swap(arr, i, k - 1);
                } else {
                    swap(arr, 0, k - 1);
                }
                permutations(k - 1, arr, result);
            }
        }
    }

    private void swap(int[] input, int a, int b) {
        int tmp = input[a];
        input[a] = input[b];
        input[b] = tmp;
    }

    private long runPhaseSettings(long[] memory, int[] phases) {
        BlockingQueue<Long> ABQ = new LinkedBlockingQueue<>();
        BlockingQueue<Long> BCQ = new LinkedBlockingQueue<>();
        BlockingQueue<Long> CDQ = new LinkedBlockingQueue<>();
        BlockingQueue<Long> DEQ = new LinkedBlockingQueue<>();
        BlockingQueue<Long> feedbackQueue = new LinkedBlockingQueue<>();

        try{
            feedbackQueue.put((long) phases[0]);
            feedbackQueue.put(0L);
            ABQ.put((long) phases[1]);
            BCQ.put((long) phases[2]);
            CDQ.put((long) phases[3]);
            DEQ.put((long) phases[4]);

            Computer ampA = new Computer(memCpy(memory), feedbackQueue, ABQ);
            Computer ampB = new Computer(memCpy(memory), ABQ, BCQ);
            Computer ampC = new Computer(memCpy(memory), BCQ, CDQ);
            Computer ampD = new Computer(memCpy(memory), CDQ, DEQ);
            Computer ampE = new Computer(memCpy(memory), DEQ, feedbackQueue);

            executor.execute(ampA::execute);
            executor.execute(ampB::execute);
            executor.execute(ampC::execute);
            executor.execute(ampD::execute);

            Future<?> ampETask = executor.submit(ampE::execute);
            ampETask.get();

            return feedbackQueue.take();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return 0;
    }

    private long[] memCpy(long[] mem) {
        return Arrays.copyOf(mem, mem.length);
    }
}
