package aoc.day10;

import aoc.AbstractDay;
import util.FileUtil;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class Day_10 extends AbstractDay {
    public static void main(String[] args) throws Exception{
        String input = FileUtil.loadResource("day10.txt");
        Day_10 day = new Day_10();
        day.test(10, input);
    }

    private Point station;

    private Set<Point> parseInput(String input) {
        Set<Point> asteriods = new HashSet<>();
        String[] lines = input.split("\n");
        for(int y = 0; y < lines.length; y++) {
            char[] chars = lines[y].toCharArray();
            for(int x = 0; x < chars.length; x++) {
                if(chars[x] == '#') {
                    asteriods.add(new Point(x * 1000, y * 1000));
                }
            }
        }
        return asteriods;
    }

    @Override
    public void part1(String input) {
        Set<Point> asteriods = parseInput(input);

        AbstractMap.SimpleEntry<Point, Long> bestLocationForStation = asteriods.stream() //
                .map(stationCandidate -> new AbstractMap.SimpleEntry<>(stationCandidate, asteriods.stream() //
                        .map(asteriod -> toVector(stationCandidate, asteriod)) //
                        .filter(Vector::isNotNullVector) //
                        .map(Vector::getUnitVector) //
                        .distinct() //
                        .count())) //
                .max(Comparator.comparingLong(AbstractMap.SimpleEntry::getValue))
                .get();

        station = bestLocationForStation.getKey();

        System.out.printf("Number of asteroids visible on %s is %d%n", bestLocationForStation.getKey(), bestLocationForStation.getValue());
    }

    public Vector toVector(Point p, Point q) {
        return  new Vector(q.x - p.x, q.y - p.y);
    }

    @Override
    public void part2(String input) {
        Set<Point> asteriods = parseInput(input);
        asteriods.remove(this.station);

        Map<Point, Vector> vectors = asteriods.stream() //
            .map(asteroid -> new AbstractMap.SimpleEntry<>(asteroid, toVector(this.station, asteroid))) //
            .filter(entry -> entry.getValue().isNotNullVector()) //
            .collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));

        Map<Vector, LinkedList<Point>> targets = new HashMap<>();
        for(Map.Entry<Point, Vector> entry : vectors.entrySet()) {
            targets.computeIfAbsent(entry.getValue().getUnitVector(), k -> new LinkedList<>()).add(entry.getKey());
        }

        targets.values().forEach(list -> list.sort(Comparator.comparing(vectors::get)));

        List<Vector> directions = new ArrayList<>(targets.keySet());
        directions.sort(Comparator.comparing(Vector::getDirection));

        while(!directions.get(0).equals(new Vector(0, -1000))) {
            Collections.rotate(directions, -1);
        }

        Point targetAsteroid = targets.get(directions.get(199)).get(0);

        System.out.printf("Solution is %d%n", (targetAsteroid.x / 1000) * 100 + (targetAsteroid.y / 1000));
    }

    public static class Vector implements Comparable<Vector>{
        private static final double epsilon = 0.001;
        public final int x;
        public final int y;
        private double length = -1;

        public boolean isNotNullVector() {
            return (x != 0 || y != 0);
        }

        public Vector(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public double getLength() {
            if(length == -1) {
                length = Math.sqrt(x * x + y * y);
            }

            return length;
        }

        public double getDirection() {
            Vector rotate = rotate();
            double rad = Math.atan2(rotate.x, rotate.y);
            return Math.PI * 2 - (rad < 0 ? rad + 2 * Math.PI : rad);
        }

        private static final double cosTheta = Math.cos(Math.PI / 2);
        private static final double sinTheta = Math.sin(Math.PI / 2);

        private Vector rotate() {
            double oldX = x / 1000.0;
            double oldY = y / 1000.0;
            int newX = (int)(1000 * (oldX * cosTheta - oldY * sinTheta));
            int newY = (int)(1000 * (oldX * sinTheta + oldY * cosTheta));
            return new Vector(newX, newY);
        }

        public Vector getUnitVector() {
            return new Vector((int)(1000 * ((double)x / getLength())), (int)(1000 * (double)y / getLength()));
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Vector vector = (Vector) o;

            if (x != vector.x) return false;
            return  (y == vector.y);
        }

        @Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            return result;
        }

        @Override
        public String toString() {
            return String.format("{%d, %d} -> %.03f°%n", x, y, Math.toDegrees(getDirection())); }

        @Override
        public int compareTo(Vector o) {
            int res = Integer.compare(this.x, o.x);

            if(res == 0){
                return Integer.compare(this.y, o.y);
            }
            return res;
        }
    }
}
