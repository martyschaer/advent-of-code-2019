package aoc.day13;

import aoc.AbstractDay;
import computer.Computer;
import computer.ExitCode;
import util.FileUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * There are 270 block tiles on screen.
 * ---
 * Day 13, Part 1: 2.050 millis
 * ---
 * Beat the game and scored 12535!
 * ---
 * Day 13, Part 2: 92.020 millis
 * ---
 */
public class Day_13 extends AbstractDay {

    public static void main(String[] args) throws Exception {
        String input = FileUtil.loadResource("day13.txt");
        Day_13 day = new Day_13();
        day.test(13, input);
    }

    private final Map<Long, Long> tiles = new HashMap<>();

    @Override
    public void part1(String input) {
        runArcadeWithoutCoins(input);
        long count = tiles.values().stream() //
                .filter(id -> Tile.BLOCK == id) //
                .count();
        System.out.printf("There are %d block tiles on screen.%n", count);
    }

    private void runArcadeWithoutCoins(String input) {
        long[] program = parseInput(input);
        BlockingQueue<Long> inputs = new LinkedBlockingQueue<>();
        BlockingQueue<Long> output = new LinkedBlockingQueue<>();
        Computer arcade = new Computer(program, inputs, output, true);

        arcade.execute();

        while (!output.isEmpty()) {
            try {
                Long x = output.take();
                Long y = output.take();
                Long tileId = output.take();

                drawTile(x, y, tileId);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void drawTile(long x, long y, long tileId) {
        tiles.put(x * 128 + y, tileId);
    }

    @Override
    public void cleanupBetweenParts() {
        this.tiles.clear();
        this.state = new State();
    }

    @Override
    public void part2(String input) {
        runArcadeWithCoins(input);
    }

    private State state = new State();

    private void runArcadeWithCoins(String input) {
        long[] program = parseInput(input);
        program[0] = 2L; // insert coins
        BlockingQueue<Long> inputs = new LinkedBlockingQueue<>();
        BlockingQueue<Long> output = new LinkedBlockingQueue<>();
        Computer arcade = new Computer(program, inputs, output, true);

        while (true) {
            int exitCode = arcade.execute();
            int outputSize = output.size();
            List<Long> out = new ArrayList<>(outputSize);
            output.drainTo(out, outputSize);

            for (int i = 0; i < out.size(); ) {
                Long x = out.get(i++);
                Long y = out.get(i++);
                Long tileId = out.get(i++);

                if (x == -1) {
                    this.state.score = tileId;
                } else {
                    updateState(x, y, tileId);
                }

            }

            if (exitCode == ExitCode.INPUT_INTERRUPT) {
                if (this.state.paddleXPos == this.state.ballXPos || this.state.paddleXPos == -1 || this.state.ballXPos == -1) {
                    inputs.add(JoyStick.NEUTRAL);
                } else if (this.state.paddleXPos < this.state.ballXPos) {
                    inputs.add(JoyStick.RIGHT);
                } else {
                    inputs.add(JoyStick.LEFT);
                }
            } else if (exitCode == ExitCode.HALT) {
                break;
            }
        }

        System.out.printf("Beat the game and scored %d!%n", this.state.score);
    }

    private void updateState(long x, long y, long tileId) {
        if(tileId == Tile.PADDLE) {
            this.state.paddleXPos = x;
        } else if(tileId == Tile.BALL) {
            this.state.ballXPos = x;
        }
        tiles.put(x * 128 + y, tileId);
    }

    private long[] parseInput(String input) {
        String[] tokens = input.trim().split(",");
        long[] memory = new long[tokens.length];
        for (int i = 0; i < tokens.length; i++) {
            memory[i] = Long.parseLong(tokens[i]);
        }

        return memory;
    }

    private static class State {
        long ballXPos = -1;
        long paddleXPos = -1;
        long score = 0;
    }

    private static class JoyStick {
        static long NEUTRAL = 0L;
        static long LEFT = -1L;
        static long RIGHT = 1L;
    }

    private static class Tile {
        static int EMPTY = 0;
        static int WALL = 1;
        static int BLOCK = 2;
        static int PADDLE = 3;
        static int BALL = 4;
    }
}
