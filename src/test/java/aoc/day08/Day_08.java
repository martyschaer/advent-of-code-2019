package aoc.day08;

import aoc.AbstractDay;
import util.FileUtil;

import java.util.Arrays;

public class Day_08 extends AbstractDay {
    public static void main(String[] args) {
        String input = FileUtil.loadResource("day08.txt");
        Day_08 day = new Day_08();
        day.test(8, input);
    }

    private static final int width = 25;
    private static final int height = 6;
    private static final int layerSize = width * height;

    @Override
    public void part1(String input) {
        char[] data = input.toCharArray();

        int numberOfLayers = data.length / layerSize;

        int[] numberOfZeroDigitsPerLayer = new int[numberOfLayers];
        int[] numberOfOneDigitsPerLayer = new int[numberOfLayers];
        int[] numberOfTwoDigitsPerLayer = new int[numberOfLayers];

        for(int i = 0; i < data.length; i++) {
            int layerNumber = i / layerSize;
            switch (data[i]) {
                case '0':
                    ++numberOfZeroDigitsPerLayer[layerNumber];
                    break;
                case '1':
                    ++numberOfOneDigitsPerLayer[layerNumber];
                    break;
                case '2':
                    ++numberOfTwoDigitsPerLayer[layerNumber];
                    break;
            }
        }

        int layerWithFewestZeros = Arrays.stream(numberOfZeroDigitsPerLayer).min().getAsInt();
        System.out.printf("Checksum for SIF = %d%n", numberOfOneDigitsPerLayer[layerWithFewestZeros] * numberOfTwoDigitsPerLayer[layerWithFewestZeros]);
    }

    @Override
    public void part2(String input) {
        char[] data = input.toCharArray();
        char[] img = new char[layerSize];

        for(int i = data.length - 1; i >= 0; --i) {
            if(data[i] != '2') {
                img[(i % layerSize)] = data[i] == '0' ? ' ' : '0'; // white=@, black=0x20
            }
        }

        StringBuilder sb = new StringBuilder();
        for(int y = 0; y < height; y++) {
            for(int x = 0; x < width; x++) {
                sb.append(img[x + y * width]);
            }
            sb.append("\n");
        }
        System.out.println(sb.toString());
    }
}
