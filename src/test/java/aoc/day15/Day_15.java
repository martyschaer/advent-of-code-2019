package aoc.day15;

import aoc.AbstractDay;
import computer.Computer;
import computer.ExitCode;
import util.FileUtil;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Day_15 extends AbstractDay {
    public static void main(String[] args) throws Exception {
        String input = FileUtil.loadResource("day15.txt");
        Day_15 day = new Day_15();
        day.test(15, input, true);
    }

    private long[] parseInput(String input) {
        String[] tokens = input.trim().split(",");
        long[] memory = new long[tokens.length];
        for (int i = 0; i < tokens.length; i++) {
            memory[i] = Long.parseLong(tokens[i]);
        }

        return memory;
    }

    private Map<Integer, Integer> map = new HashMap<>();

    @Override
    public void part1(String input) {
        long[] program = parseInput(input);
        BlockingQueue<Long> inputs = new LinkedBlockingQueue<>();
        BlockingQueue<Long> output = new LinkedBlockingQueue<>();
        Computer droid = new Computer(program, inputs, output, true);
        inputs.add(Directions.NORTH);
        Point botPos = new Point(0, 0);
        Point proposedbotPos = new Point(0, 1);
        long lastDirection = Directions.NORTH;
        long nextDirection = Directions.NORTH;
        try {
            while (true) {
                int exitCode = droid.execute();
                if (exitCode == ExitCode.INPUT_INTERRUPT) {
                    Long status = output.take();
                    if(status.equals(Status.OK)) {
                        botPos.x = proposedbotPos.x;
                        botPos.y = proposedbotPos.y;
                        setTerrain(botPos.x, botPos.y, Terrain.FLOOR);
                    } else if(status.equals(Status.NOK)) {
                        setTerrain(proposedbotPos.x, proposedbotPos.y, Terrain.WALL);
                    } else if(status.equals(Status.TARGET)) {
                        break;
                    }
                    lastDirection = nextDirection(botPos.x, botPos.y, lastDirection);
                    inputs.add(lastDirection);
                    render(botPos);
                } else if (exitCode == ExitCode.HALT) {
                    break;
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void dfs(Computer droid, int direction) {

    }

    private long nextDirection(int x, int y, long lastDirection) {
        long opposite = Directions.getOppositeDirection(lastDirection);
        if(opposite != Directions.NORTH && getTerrain(x, y + 1) != Terrain.WALL) {
            return Directions.NORTH;
        } else if(opposite != Directions.SOUTH && getTerrain(x, y - 1) != Terrain.WALL) {
            return Directions.SOUTH;
        } else if(opposite != Directions.EAST && getTerrain(x + 1, y) != Terrain.WALL) {
            return Directions.EAST;
        } else if(opposite != Directions.WEST && getTerrain(x - 1, y) != Terrain.WALL) {
            return Directions.WEST;
        } else {
            return opposite; // no other paths, backtrack
        }
    }

    private void render(Point botPos) {
        int maxX = map.keySet().stream().mapToInt(x -> x / 1024).max().getAsInt();
        int minX = map.keySet().stream().mapToInt(x -> x / 1024).min().getAsInt();
        int maxY = map.keySet().stream().mapToInt(x -> x % 1024).max().getAsInt();
        int minY = map.keySet().stream().mapToInt(x -> x % 1024).min().getAsInt();

        StringBuilder sb = new StringBuilder();
        for (int y = minY; y < maxY; y++) {
            for (int x = minX; x < maxX; x++) {
                if(botPos.x == x && botPos.y == y) {
                    sb.append("D");
                    continue;
                }
                switch (getTerrain(x, y)) {
                    case Terrain.NIL:
                        sb.append(" ");
                        break;
                    case Terrain.FLOOR:
                        sb.append(".");
                        break;
                    case Terrain.WALL:
                        sb.append("#");
                        break;
                }
            }
            sb.append("\n");
        }
        System.out.println(sb.toString());
    }

    private void setTerrain(int x, int y, int terrain) {
        map.put(x * 1014 + y, terrain);
    }

    private int getTerrain(int x, int y) {
        return map.computeIfAbsent(x * 1024 + y, k -> Terrain.NIL);
    }

    @Override
    public void part2(String input) {

    }

    private static class Terrain {
        public static final int NIL = 0;
        public static final int WALL = 1;
        public static final int FLOOR = 2;
    }

    private static class Status {
        public static final Long NOK = 0L;
        public static final Long OK = 1L;
        public static final Long TARGET = 2L;
    }

    private static class Directions {
        public static final long NORTH = 1L;
        public static final long SOUTH = 2L;
        public static final long WEST = 3L;
        public static final long EAST = 4L;
        private static final Point up = new Point(0, -1);
        private static final Point down = new Point(0, 1);
        private static final Point left = new Point(-1, 0);
        private static final Point right = new Point(1, 0);

        public static long getOppositeDirection(long dir) {
            switch ((int)dir) {
                case (int) NORTH: return SOUTH;
                case (int) SOUTH: return NORTH;
                case (int) WEST: return EAST;
                case (int) EAST: return WEST;
            }
            throw new IllegalArgumentException("Couldn't determine opposite direction");
        }

        public static Point getMove(long dir) {
            switch ((int)dir) {
                case (int) NORTH: return up;
                case (int) SOUTH: return down;
                case (int) WEST: return left;
                case (int) EAST: return right;
            }
            throw new IllegalArgumentException("Couldn't determine opposite direction");
        }
    }
}
