package aoc;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.IntStream;

public class Sandchaschte {
    public static void main(String[] args) {
        IntStream.rangeClosed(0, 100).forEach(r -> System.out.println(roundUpBy(r, 5)));
    }

    private static int roundUpBy(int toRound, double increment) {
        return (int) (increment * Math.ceil(toRound / increment));
    }
}
