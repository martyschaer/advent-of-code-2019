package aoc.day02;

import aoc.AbstractDay;
import day2.OpCode;
import day2.Operation;
import util.FileUtil;

import java.util.Arrays;

public class Day_02 extends AbstractDay {
    public static void main(String[] args) throws Exception{
        String input = FileUtil.loadResource("day02.txt");
        Day_02 day = new Day_02();
        day.test(2, input);
    }

    private static int HLT = 99;

    @Override
    public void part1(String input) {
        String[] tokens = input.trim().split(",");
        int[] memory = new int[tokens.length];
        for(int i = 0; i < tokens.length; i++) {
            memory[i] = Integer.parseInt(tokens[i]);
        }

        // reset memory to 1202 state
        memory[1] = 12;
        memory[2] = 2;

        for(int ip = 0; ip < memory.length; ip += 4) {
            int opCode = memory[ip];
            if(HLT == opCode) {
                System.out.println("Received HLT instruction! memory[0]=" + memory[0]);
                return;
            }

            Operation operation = new Operation(OpCode.ofVal(opCode),
                    memory[ip + 1],
                    memory[ip + 2],
                    memory[ip + 3]);

            operation.execute(memory);
        }
    }

    @Override
    public void part2(String input) {
        String[] tokens = input.trim().split(",");
        int[] memory = new int[tokens.length];
        for(int i = 0; i < tokens.length; i++) {
            memory[i] = Integer.parseInt(tokens[i]);
        }

        int[] initialMemory = Arrays.copyOf(memory, memory.length);


        for(int noun = 0; noun <= 99; noun++) {
            for(int verb = 0; verb <= 99; verb++) {
                if(compute(noun, verb, Arrays.copyOf(initialMemory, initialMemory.length))) {
                    return;
                }
            }
        }
    }

    private boolean compute(int noun, int verb, int[] memory) {
        memory[1] = noun;
        memory[2] = verb;

        for(int ip = 0; ip < memory.length; ip += 4) {
            int opCode = memory[ip];
            if(HLT == opCode) {
                if(memory[0] == 19690720) {
                    System.out.printf("Reached desired output. Noun=%d, Verb=%d, Solution=%d%n",
                            noun, verb, 100 * noun + verb);
                    return true;
                } else {
                    return false;
                }
            }

            Operation operation = new Operation(OpCode.ofVal(opCode),
                    memory[ip + 1],
                    memory[ip + 2],
                    memory[ip + 3]);

            operation.execute(memory);
        }

        return false;
    }
}
