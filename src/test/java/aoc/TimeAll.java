package aoc;

import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;
import util.FileUtil;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class TimeAll {
    private static Map<Integer, String> inputs = new ConcurrentHashMap<>();

    public static void main(String[] args) throws Exception {
        int iterations = 15;
        int solvedDays = 13;

        Duration fullDuration = Duration.ZERO;

        OutputStream out = new ByteOutputStream();
        System.setOut(new PrintStream(out));

        for(int i = 0; i < iterations; i++) {
            System.err.printf("Running iteration %d...%n", i);
            for (int day = 1; day <= solvedDays; day++) {
                System.err.printf("\tRunning day %d...%n", day);
                AbstractDay instance = TimeAll.loadInstance(day);
                String input =  inputs.computeIfAbsent(day, k -> FileUtil.loadResource("day" + String.format("%02d", k) + ".txt"));
                Instant start = Instant.now();
                instance.part1(input);
                instance.cleanupBetweenParts();
                instance.part2(input);
                Instant end = Instant.now();
                instance.tearDown();
                fullDuration = fullDuration.plus(Duration.between(start, end));
            }
        }

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));

        System.out.printf("Ran all days %d times, taking an average of %s.%n", iterations, Util.toHumanReadable(fullDuration.dividedBy(iterations)));
    }

    private static AbstractDay loadInstance(int day) {
        try {
            String d = String.format("%02d", day);
            return  (AbstractDay) Class.forName("aoc.day" + d + ".Day_" + d).newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
