package aoc.day16;

import aoc.AbstractDay;
import util.FileUtil;

import java.util.*;
import java.util.stream.Collectors;

public class Day_16 extends AbstractDay {
    public static void main(String[] args) throws Exception{
        String input = FileUtil.loadResource("day16.txt");
        Day_16 day = new Day_16();
        day.test(16, input, true);
    }

    private List<Integer> parseInput(String input) {
        return Arrays.stream(input.split("")) //
            .map(Integer::parseInt) //
            .collect(Collectors.toList());
    }

    @Override
    public void part1(String raw) {
        List<Integer> basePattern = Arrays.asList(0, 1, 0, -1);
        List<Integer> input = parseInput(raw);
        for(int i = 0; i < 100; i++) {

        }
    }

    List<Integer> fftPhase(List<Integer> input, List<Integer> basePattern) {
        List<Integer> output = new LinkedList<>();
        for(int i = 0; i < input.size(); i++) {
            List<Integer> pattern = generatePattern(basePattern, i);
            Collections.rotate(pattern, -1);
            int result = 0;
            for(int x = 0; x < input.size(); x++) {
                result += input.get(x) * pattern.get(x % pattern.size());
            }
            output.add(result % 10);
        }
        return output;
    }

    List<Integer> generatePattern(List<Integer> basePattern, int position) {
        List<Integer> output = new LinkedList<>();
        int ordinal = position + 1;
        for(int i = 0; i < basePattern.size() * ordinal; i++) {
            output.add(basePattern.get(i / ordinal));
        }
        return output;
    }

    @Override
    public void part2(String input) {

    }
}
